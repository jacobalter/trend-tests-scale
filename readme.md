To run:
1.) If running BP, you may need to run pull_bigquery_data.R first to get the data.
2.) Update the indicated variables in the run_trend_tests_*.R file.
3.) Run the respective script for that survey program.
