data {
  int<lower=1> N;
  int<lower=1> K;
  matrix[K, N] y;
  vector[N] x;
}

parameters {
  real alpha;
  real beta;
  real phi_fisher;
  real log_sigma_innov;

  real<lower = 0> sigma_alpha;
  real<lower = 0> sigma_beta;
  real<lower = 0> sigma_phi_fisher;
  real<lower = 0> sigma_log_sigma_innov;

  matrix[4, K] z_params;
}

transformed parameters {
  real phi = tanh(phi_fisher);
  real sigma_innov = exp(log_sigma_innov);

  vector[K] alpha_k;
  vector[K] beta_k;
  vector[K] phi_k;
  vector[K] sigma_innov_k;
  {
    matrix[4, K] params;
    matrix[4, 4] Sigma;
    vector[4] mu;

    mu = [alpha, beta, phi_fisher, log_sigma_innov]';
    Sigma = diag_matrix([sigma_alpha, sigma_beta, sigma_phi_fisher, sigma_log_sigma_innov]');
    params = rep_matrix(mu, K) + Sigma * z_params;

    alpha_k = params[1]';
    beta_k = params[2]';
    phi_k = tanh(params[3])';
    sigma_innov_k = exp(params[4])';
  }
}

model {
  alpha ~ normal(50, 15);
  sigma_alpha ~ normal(0, 20);

  beta ~ normal(0, 0.25);
  sigma_beta ~ normal(0, 0.5);

  phi_fisher ~ normal(0, 0.3);
  sigma_phi_fisher ~ normal(0, 0.2);

  log_sigma_innov ~ normal(log(2), 0.5);
  sigma_log_sigma_innov ~ normal(0, 0.25);

  to_vector(z_params) ~ std_normal();

  for (k in 1:K) {
    vector[N-1] mu;

    mu = phi_k[k] * y[k, :(N-1)]' +
        (1 - phi_k[k]) * (alpha_k[k] + beta_k[k] * x[:(N-1)]) +
        beta_k[k];

    y[k, 1] ~ normal(alpha_k[k], sigma_innov_k[k] / sqrt(1 - phi_k[k]^2));
    y[k, 2:] ~ normal(mu, sigma_innov_k[k]);
  }
}
