data {
  int<lower=1> N_series;
  int<lower=1> T;

  vector[T] y_measured[N_series];
  vector<lower=0>[T] weighted_base[N_series];

  int N_prod;
  int<lower=1, upper=N_prod> product_index[N_series];

  int N_metric;
  int<lower=1, upper=N_metric> metric_index[N_series];
}

transformed data {
  vector[T] measurement_sd[N_series];
  for (n in 1:N_series) {
    measurement_sd[n] = sqrt(y_measured[n] .* (1 - y_measured[n]) ./ weighted_base[n]);
  }
}

parameters {
  vector[T-1] z_err[N_series];
  vector[T-1] z_product[N_prod];

  real err_intercept[N_series];

  real product_intercept[N_prod];
  real metric_intercept[N_metric];
  real<lower=0, upper=1> metric_slope[N_metric];

  real<lower=0> sigma_err_rw;
  real<lower=0> sigma_product_rw;
}

transformed parameters {
  vector[T] product_effect[N_prod];
  vector[T] y[N_series];

  for (n in 1:N_prod) {
    product_effect[n, 1] = product_intercept[n];
    product_effect[n, 2:T] = product_intercept[n] + sigma_product_rw * cumulative_sum(z_product[n]);
  }

  for (n in 1:N_series) {
    vector[T] err;
    err[1] = err_intercept[n];
    err[2:T] = err_intercept[n] + sigma_err_rw * cumulative_sum(z_err[n]);

    y[n] = metric_slope[metric_index[n]] * product_effect[product_index[n]] +
      metric_intercept[metric_index[n]] + err;
  }
}

model {
  for (n in 1:N_series) {
    z_err[n] ~ std_normal();
  }

  for (n in 1:N_prod) {
    z_product[n] ~ std_normal();
  }

  sigma_product_rw ~ normal(0, 0.005);
  sigma_err_rw ~ normal(0, 0.005);

  to_array_1d(err_intercept) ~ normal(0, 0.05);
  metric_intercept ~ normal(0.5, 0.3);
  product_intercept ~ normal(0, 0.1);
  metric_slope ~ beta(5, 1);

  for (n in 1:N_series) {
    y_measured[n] ~ normal(y[n], measurement_sd[n]);
  }
}

