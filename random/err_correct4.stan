functions {
  matrix random_walk_compose(real[] intercept, vector[] z, real sigma) {
    int N = size(intercept);
    int T = num_elements(z[1]) + 1;
    matrix [N, T] rw;

    for (n in 1:N) {
      rw[n, 1] = intercept[n];
      rw[n, 2:T] = (intercept[n] + sigma * cumulative_sum(z[n]))';
    }
    return rw;
  }

  matrix get_orthogonal_transform(int K) {
    matrix[K, K] A = diag_matrix(rep_vector(1, K));
    matrix[K, K-1] A_qr;

    for (i in 1:K-1) {
      A[K, i] = -1;
    }
    A[K, K] = 0;
    A_qr = qr_Q(A)[, 1:(K-1)] * sqrt(K * inv(K-1));

    return A_qr;
  }
}

data {
  int<lower=1> N_series;
  int<lower=1> T;

  vector[T] y_measured[N_series];
  vector<lower=0>[T] weighted_base[N_series];

  int N_prod;
  int<lower=1, upper=N_prod> product_index[N_series];

  int N_metric;
  int<lower=1, upper=N_metric> metric_index[N_series];
}

transformed data {
  vector[T] measurement_sd[N_series];
  matrix[N_prod, N_prod-1] Q_prod = get_orthogonal_transform(N_prod);
  matrix[N_metric, N_metric-1] Q_metric = get_orthogonal_transform(N_metric);
  int total_obs = N_series * T;
  for (n in 1:N_series) {
    measurement_sd[n] = sqrt(y_measured[n] .* (1 - y_measured[n]) ./ weighted_base[n]);
  }
}

parameters {
  real grand_mean;

  vector<lower=0, upper=1>[N_metric] metric_slope;

  vector[T-1] z_err[N_series];
  vector[T-1] z_product_raw[N_prod-1];
  vector[T-1] z_metric_raw[N_metric-1];

  real err_intercept[N_series];
  real product_intercept_raw[N_prod-1];
  real metric_intercept_raw[N_metric-1];

  real<lower=0> sigma_err_rw;
  real<lower=0> sigma_product_rw;
  real<lower=0> sigma_metric_rw;
}

transformed parameters {
  matrix[N_series, T] err;
  matrix[N_prod, T] product_effect;
  matrix[N_metric, T] metric_effect;

  matrix[N_series, T] y;

  product_effect = Q_prod * random_walk_compose(product_intercept_raw, z_product_raw, sigma_product_rw);
  metric_effect = Q_metric * random_walk_compose(metric_intercept_raw, z_metric_raw, sigma_metric_rw);
  err = random_walk_compose(err_intercept, z_err, sigma_err_rw);

  y = rep_matrix(grand_mean, N_series, T) +
    rep_matrix(metric_slope[metric_index], T) .* product_effect[product_index] +
    metric_effect[metric_index] + err;
}

model {
  for (n in 1:N_series) {
    z_err[n] ~ std_normal();
  }

  for (n in 1:N_prod-1) {
    z_product_raw[n] ~ std_normal();
  }

  for (n in 1:N_metric-1) {
    z_metric_raw[n] ~ std_normal();
  }

  sigma_err_rw ~ normal(0, 0.01);
  sigma_product_rw ~ normal(0, 0.005);
  sigma_metric_rw ~ normal(0, 0.005);

  err_intercept ~ normal(0, 0.15);
  metric_intercept_raw ~ normal(0, 0.3);
  product_intercept_raw ~ normal(0, 0.1);

  metric_slope ~ beta(5, 1);

  grand_mean ~ normal(0.5, 0.2);

  for (n in 1:N_series) {
    y_measured[n] ~ student_t(weighted_base[n]-1, y[n], measurement_sd[n]);
  }
}

generated quantities {
  real y_rep[N_series, T];

  for (n in 1:N_series) {
    y_rep[n] = student_t_rng(weighted_base[n]-1, y[n], measurement_sd[n]);
  }
}
