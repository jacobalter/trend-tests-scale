data {
  int<lower=1> N_series;
  int<lower=1> T;

  vector[T] y_measured[N_series];
  vector<lower=0>[T] weighted_base[N_series];

  int N_prod;
  int<lower=1, upper=N_prod> product_index[N_series];

  int N_metric;
  int<lower=1, upper=N_metric> metric_index[N_series];
}

transformed data {
  vector[T] measurement_sd[N_series];
  matrix[N_prod, N_prod-1] A_qr;
  for (n in 1:N_series) {
    measurement_sd[n] = sqrt(y_measured[n] .* (1 - y_measured[n]) ./ weighted_base[n]);
  }
  {
    matrix[N_prod, N_prod] A = diag_matrix(rep_vector(1, N_prod));
    for (i in 1:N_prod-1) {
      A[N_prod, i] = -1;
    }
    A[N_prod, N_prod] = 0;
    A_qr = qr_Q(A)[, 1:(N_prod-1)] * sqrt(N_prod * inv(N_prod-1));
  }
}

parameters {
  vector[T-1] z_err[N_series];
  vector[T-1] z_product_raw[N_prod-1];

  real err_intercept[N_series];

  real product_intercept_raw[N_prod-1];
  real metric_intercept[N_metric];
  real<lower=0, upper=1> metric_slope[N_metric];

  real<lower=0> sigma_err_rw;
  real<lower=0> sigma_product_rw;
}

transformed parameters {
  matrix[N_prod, T] product_effect;
  vector[T] y[N_series];

  {
    matrix [N_prod-1, T] product_effect_raw;

    for (n in 1:N_prod-1) {
      product_effect_raw[n, 1] = product_intercept_raw[n];
      product_effect_raw[n, 2:T] =
        (product_intercept_raw[n] + sigma_product_rw * cumulative_sum(z_product_raw[n]))';
    }

    product_effect = A_qr * product_effect_raw;
  }

  for (n in 1:N_series) {
    vector[T] err;
    err[1] = err_intercept[n];
    err[2:T] = err_intercept[n] + sigma_err_rw * cumulative_sum(z_err[n]);

    y[n] = metric_slope[metric_index[n]] * product_effect[product_index[n]]' +
      metric_intercept[metric_index[n]] + err;
  }
}

model {
  for (n in 1:N_series) {
    z_err[n] ~ std_normal();
  }

  for (n in 1:N_prod-1) {
    z_product_raw[n] ~ std_normal();
  }

  sigma_product_rw ~ normal(0, 0.005);
  sigma_err_rw ~ normal(0, 0.005);

  to_array_1d(err_intercept) ~ normal(0, 0.05);
  metric_intercept ~ normal(0.5, 0.3);
  product_intercept_raw ~ normal(0, 0.1);
  metric_slope ~ beta(5, 1);

  for (n in 1:N_series) {
    y_measured[n] ~ student_t(weighted_base[n]-1, y[n], measurement_sd[n]);
  }
}

