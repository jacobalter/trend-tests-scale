data {
  int<lower=0> N;
  int<lower=0> N_obs;
  int index_obs[N_obs];
  vector[N_obs] y_measured;
  vector<lower=0>[N_obs] sd_measurement;
}

parameters {
  real alpha;
  real<lower=0> sigma;
  vector[N-1] z;
}

transformed parameters {
  vector[N] y_true;
  y_true[1] = alpha;
  y_true[2:N] = alpha + sigma * cumulative_sum(z);
}

model {
  z ~ std_normal();
  alpha ~ normal(0.5, 0.5);
  sigma ~ cauchy(0, 0.01);

  y_measured ~ normal(y_true[index_obs], sd_measurement);
}

