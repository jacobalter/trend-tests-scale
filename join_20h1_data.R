TEENS_CATEGORIES <- c("13-15", "16-17", "Teens Female", "Teens Male", "Teens Total")

fix_subgroup_names <- function(s) {
  exceptions <- s %in% c("Adults Male", "Adults Female", "Adults Total")
  if_else(
    exceptions,
    s,
    str_remove(s, "Adults ")
  )
}

df <- fread("data/usat_h_2019h2_longfile_ISC_merged.txt", strip.white = FALSE)

df[,subgroup := str_trim(subgroup)]

df2 <- readxl::read_excel("data/usat_h_2020h1_20200604.xlsx", sheet = 1)

df2 <- df2 %>% mutate(
  subgroup = fix_subgroup_names(subgroup),
  unweighted_small_base = replace_na(unweighted_small_base, ""),
  weighted_small_base = replace_na(weighted_small_base, "")
)


final <- rbind(df, df2, fill = TRUE)

fwrite(final, "data/usat_2020h1.csv", na = "", bom = TRUE)
