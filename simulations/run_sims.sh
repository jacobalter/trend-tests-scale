#!/bin/sh

for method in OLS GLS NW BGNW; do
	echo $method
	for phi in 0 0.2 -0.2; do
		echo $phi
		for n in 4 13 100; do
			echo $n
			fname=sim_plots/phist_phi=${phi}_n=${n}_method=${method}.png
			Rscript neweywest_sims.R --method=$method --phi=$phi --n=$n --fname=$fname
		done
	done
done
