#!/bin/sh

for n in 4 13 100; do
	for phi in 0 0.2 -0.2; do
		fname=sim_plots/bg_plots/phist_phi=${phi}_n=${n}.png
		Rscript bg_sims.R --n=$n --phi=$phi --fname=$fname
	done
done
