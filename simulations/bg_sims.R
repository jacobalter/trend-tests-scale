library(optparse)

option_list = list(
  make_option("--slope", type ="double", default = 0.0,
              help = "slope of line"),
  make_option("--n", type = "integer", default = 4,
              help = "length of series"),
  make_option("--phi", type = "double", default = 0.0,
              help = "autocorrelation of residuals"),
  make_option("--sigma", type = "double", default = 0.02,
              help = "standard deviation of residuals"),
  make_option("--fname", type = "character", default = NULL,
              help = "filename to save plot")
)

opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

nrep <- 10000 # number of replicates
beta <- opt$slope # slope of regression line
alpha <- 0.3 # intercept of regression line
sigma_res <- opt$sigma # standard deviation of residuals
phi <- opt$phi # autocorrelation of residuals
n <- opt$n # length of series

# filename for plot
fname <- opt$fname

x <- 0:(n-1)
sigma_innov <- sigma_res * sqrt(1 - phi*phi)

p <- numeric(nrep)
for (i in 1:nrep) {
  if (phi == 0) {
    epsilon <- rnorm(n, sd = sigma_res)
  } else {
    epsilon <- arima.sim(
      model = list(ar = phi), n = n,
      rand.gen = function(n) rnorm(n, sd = sigma_innov)
    )
  }

  y <- alpha + beta * x + epsilon
  model <- lm(y ~ x)
  p[i] <- lmtest::bgtest(model, order = 1, type = "F")[["p.value"]]
}

title <- paste0("Slope=", beta, ", Autocorrelation=", phi, ", n=", n)

png(fname)
hist(
  p, breaks = seq(0, 1, by = 0.05), freq = FALSE,
  xlab = "p-value", main = title
)
abline(h = 1.0, lty = "dashed")
title(
  sub = paste0("Proportion of significant results: ", mean(p < 0.05))
)
invisible(dev.off())
