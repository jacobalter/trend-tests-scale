#!/bin/sh

for autocorr in BGNW; do
	echo $autocorr
	for weighting in TRUE FALSE; do
		echo $weighting
		fname=results/usat_19H2_weighting=${weighting}_method=${autocorr}.csv
		Rscript run_trend_tests_usat.R --weighting=$weighting --autocorr=$autocorr --fname=$fname
	done
done
