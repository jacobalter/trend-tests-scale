library(data.table)
setDTthreads(0)

# THESE SHOULD BE REVIEWED/UPDATED EVERY RUN
# location of downloaded GCP files
GCP_EXPORT_PATH <- "data/gcp_export/"
# where all the queried data will be stored
FULL_DATA_PATH <- "data/bp_21MAY.csv"
# folder where the partitioned data (separated by country) will be stored
BP_PATH <- "data/brandpulse"

# representation of null values in the CSV output
NULL_VALUE <- ""

# Example query:
# SELECT * FROM `mgcp-1192365-ipsos-gbht-srf617.Google_BrandPulse_Database.brandpulse_internal_merged_datasheet_AVRO`
# WHERE Time_Series >= "2020-02-01" AND Time_Series <= "2021-02-01"
# ORDER BY Country, Product_Category, Brand, Metric_Name, Response_Used,
#          Subgroup, Stats_Type, Base_Note, Product_Rollup, Time_Series, KTA

# When query is completed, go to query in Query History and click on "temporary table" under destination
# Export to GCS

# Example path for GCS export:
# ipsos-shared-bucket/BrandPulse/BP_TrendTest/jacob_temp/tts_21feb_*.csv.gz

# Manually download the files from GCS and move into an empty folder (specified as GCP_EXPORT_PATH)
# In command line, run "gunzip *" to extract files
# Then run this script

# Then run run_trend_tests_bp.R

frames <- list()
for (fname in list.files(GCP_EXPORT_PATH, full.names = TRUE)) {
  print(fname)
  frames[[fname]] <- fread(fname, strip.white = FALSE, na.strings = NULL_VALUE)
}
df <- rbindlist(frames)


fwrite(df, FULL_DATA_PATH, na = NULL_VALUE, bom = TRUE)

df <- as.data.table(df)
setorder(df, Country, Product_Category, Brand, Metric_Name, Response_Used, Subgroup,
         Stats_Type, Base_Note, Time_Series)

# clear out BP_PATH directory (to be safe)
for (fname in list.files(BP_PATH, full.names = TRUE, recursive = TRUE)) {
  file.remove(fname)
}

countries <- df[, unique(Country)]
for (country in countries) {
  print(country)
  fwrite(
    df[Country == country],
    file.path(BP_PATH, paste0(country, ".csv")),
    na = NULL_VALUE
  )
}
